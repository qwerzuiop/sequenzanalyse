#pragma once

#include "rankSelect.h"
#include "bwt.h"

// returns {n,n} if there is no match
// or [i,j] where [i..j] is the (inclusive) interval
// of matches in the suffix array
template<typename It, typename T = ref_type_t<It>>
std::pair<unsigned int, unsigned int> backwardSearch(const Carray<T>& carr, const RankSelects<T>& bwtRS, It pbegin, It pend) {
	const unsigned int n = bwtRS.begin()->second.n();
	unsigned int i = 0, j = n-1;
	if(pbegin == pend)
		return {n,n};
	do {
		pend--;
		const T c = *pend;

		if(carr.count(c)==0)
			return {n,n};
		
		i = carr.at(c) + bwtRS.at(c).rank(static_cast<int>(i)-1, true);
		j = carr.at(c) + bwtRS.at(c).rank(j, true) - 1;
	} while(pbegin != pend && i <= j);

	if(i<=j) {
		return {i,j};
	} else {
		return {n,n};
	}
}
