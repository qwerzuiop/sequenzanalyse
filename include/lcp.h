#pragma once

#include "util.h"
#include "rmq.h"
#include "skew.h"

#include <optional>
#include <stack>

using LCP = std::vector<int>;

// calculate lcp array using the kasai algorithm
template<typename It, typename T = ref_type_t<It>>
LCP kasai(It begin, It end, const SA&);

struct lcp_interval_tree {
	unsigned int lcp;
	size_t lb, rb;
	std::vector<lcp_interval_tree> childList;
};

// bottom-up construction of the lcp-interval tree
template<typename F>
lcp_interval_tree build_lcp_tree(const LCP& lcp, F process);
lcp_interval_tree build_lcp_tree(const LCP& lcp);

template<typename It, typename T>
LCP kasai(It begin, It end, const SA& sa) {
	const size_t n = std::distance(begin, end);
	assert(n == sa.size());
	LCP lcp(n+1, -1);

	std::vector<unsigned int> isa(n);
	for(unsigned int i=0; i<n; i++)
		isa[sa[i]] = i;
	int l = 0;
	for(unsigned int i=0; i<n; i++) {
		const auto j = isa[i];
		if(j > 0) {
			const auto k = sa[j-1];
			while(std::max(i+l, k+l) < n && *(begin+k+l) == *(begin+i+l)) l++;
			lcp[j] = l;
			l = std::max(l-1, 0);
		}
	}
	return lcp;
}

template<typename F>
lcp_interval_tree build_lcp_tree(const LCP& lcp, F process) {
	assert(lcp.size() > 1);

	std::optional<lcp_interval_tree> lastInterval;
	std::stack<lcp_interval_tree> s;
	s.emplace(lcp_interval_tree{ 0u, 0, (size_t)-1, {} });
	
	for(size_t k=1; k<lcp.size(); k++) {
		size_t lb = k-1;
		assert(!s.empty());
		while(!s.empty() && lcp[k] < (int) s.top().lcp) {
			s.top().rb = k-1;
			lastInterval = s.top(); s.pop();
			process(*lastInterval);
			lb = lastInterval->lb;
			if(!s.empty() && lcp[k] <= (int) s.top().lcp) {
				s.top().childList.emplace_back(std::move(*lastInterval));
				lastInterval = std::nullopt;
			}
		}
		if(!s.empty() && lcp[k] > (int) s.top().lcp) {
			if(lastInterval) {
				s.emplace(lcp_interval_tree{ (unsigned int) lcp[k], lb, -1ul, {std::move(*lastInterval)} });
				lastInterval = std::nullopt;
			} else
				s.emplace(lcp_interval_tree{ (unsigned int) lcp[k], lb, -1ul, {} });
		}
	}

	assert(!!lastInterval);
	return *lastInterval;
}
