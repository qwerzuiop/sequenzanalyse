#pragma once

#include <vector>
#include <cassert>

template<typename T>
class RMQ {
private:
	std::vector<T> arr;
	std::vector<std::vector<T>> b; //b[i][j]: index of smallest element in arr[i..i+2^j-1]
	size_t n;
public:
	// O(1)
	const T& at(size_t i) const {
		return arr[i];
	}
	// O(n log n)
	RMQ(const std::vector<T>& a)
			: RMQ(a.begin(), a.end()) {
	}
	// O(n log n)
	template<typename It>
	RMQ(It lo, const It& hi) {
		for(; lo != hi; lo++)
			arr.emplace_back(*lo);
		n = arr.size();
		b.resize(64-__builtin_clzll(n), std::vector<T>(n));
		
		for(size_t i=0; i<n; i++)
			b[0][i] = i;
		for(size_t j=1, len=2; len <= n; len*=2, j++){
			for(size_t i=0; i<=n-len; i++){
				size_t x1 = b[j-1][i],
				x2 = b[j-1][i+len/2];
				b[j][i] = arr[x1] < arr[x2] ? x1 : x2;
			}
		}
	}

	// index of smallest element in arr[from..to] (inclusively)
	// O(1)
	size_t query(size_t from, size_t to) const {
		assert(from <= to);
		size_t d2 = 63-__builtin_clzll(to - from + 1);
		size_t i = b[d2][from];
		size_t j = b[d2][to-(1ull<<d2)+1];
		return arr[i] <= arr[j] ? i : j;

	}
};
