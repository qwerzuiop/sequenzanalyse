#pragma once

#include "skew.h"

#include <map>

template<typename T>
std::vector<T> burrows_wheeler(const std::vector<T>&);
template<typename T>
std::vector<T> burrows_wheeler(const std::vector<T>&, const SA&);

template<typename T>
using Carray = std::map<T, unsigned int>;
template<typename It, typename T = ref_type_t<It>>
Carray<T> C_array(It begin, It end);

template<typename It, typename T = ref_type_t<It>>
std::vector<unsigned int> last_to_first(It begin, It end);

template<typename T>
std::vector<T> reconstruct_from_BWT(const std::vector<T>&);
template<typename T>
std::vector<T> reconstruct_from_BWT(const std::vector<T>&,
		const std::vector<unsigned int>&);

template<typename T>
std::vector<T> burrows_wheeler(const std::vector<T>& s) {
	const auto sa = skew(s.begin(), s.end());
	return burrows_wheeler(s, sa);
}
template<typename T>
std::vector<T> burrows_wheeler(const std::vector<T>& s, const SA& sa) {
	assert(s.size() == sa.size());
	std::vector<T> bwt(s.size());
	for(size_t i = 0; i < s.size(); i++)
		if(sa[i] == 0)
			bwt[i] = s.back();
		else
			bwt[i] = s[sa[i]-1];
	return bwt;
}

template<typename It, typename T>
Carray<T> C_array(It begin, It end) {
	std::vector<T> f(begin, end);
	bucketSort(f.begin(), f.end());

	std::map<T,unsigned int> C;
	for(unsigned int i = 0; i < f.size(); i++)
		if(i == 0 || f[i] != f[i-1])
			C[f[i]] = i;
	
	return C;
}

template<typename It, typename T>
std::vector<unsigned int> last_to_first(It begin, It end) {
	const size_t n = std::distance(begin, end);

	auto C = C_array(begin, end);
	std::vector<unsigned int> lf(n);
	for(size_t i=0; begin != end; i++,begin++) {
		const T& c = *begin;
		lf[i] = C[c]++;
	}
	return lf;
}

template<typename T>
std::vector<T> reconstruct_from_BWT(const std::vector<T>& bwt) {
	return reconstruct_from_BWT(bwt, last_to_first(bwt.begin(), bwt.end()));
}
template<typename T>
std::vector<T> reconstruct_from_BWT(const std::vector<T>& bwt,
		const std::vector<unsigned int>& lf) {
	const auto n = bwt.size();
	size_t j = std::distance(lf.begin(), std::find(lf.begin(), lf.end(), 0));
	std::vector<T> res(n);
	for(int i = n-1; i>=0; i--) {
		res[i] = bwt[j];
		j = lf[j];
	}
	return res;
}
