#pragma once

#include "util.h"

#include <algorithm>
#include <cassert>

using SA = std::vector<unsigned int>;

template<typename It, typename T = ref_type_t<It>>
void bucketSort(It begin, It end);
// buckets are numbered from 0 to n-1 (inclusively)
// callable f assigns each T in v to a bucket number
template<typename It, typename T = ref_type_t<It>, typename F>
void bucketSort(size_t n, It begin, It end, F f);
template<typename It, typename T = ref_type_t<It>>
SA skew(It begin, It end);

size_t countOcc(const std::string& s, const std::string& pat);
size_t countOcc2(const std::string& s, const std::string& pat);

template<typename It, typename T>
void bucketSort(It begin, It end) {
	// minV is an element smaller than any element in s (e.g. '$')
	const T minV = static_cast<T>(*std::min_element(begin, end) - 1);
	// maxV is the largest element in s
	const T maxV = *std::max_element(begin, end);
	assert(minV < maxV); // just make sure the type conversion was successful
	bucketSort(size_t(maxV - minV) + 1, begin, end, [minV](const T& a) { return size_t(a - minV); });
}
template<typename It, typename T, typename F>
void bucketSort(size_t n, It begin, It end, F f) {
	std::vector<std::vector<T>> buckets(n);
	for(It i=begin; i!=end; i++)
		buckets[f(*i)].emplace_back(std::move(*i));
	for(size_t i = 0; i < n; i++)
		for(auto& v : buckets[i])
			*(begin++) = std::move(v);
}

template<typename It, typename T>
SA skew(It begin, It end) {
	const size_t n = std::distance(begin, end);
	if(n == 1) { // base case
		return {0};
	}
	// minV is an element smaller than any element in s (e.g. '$')
	const T minV = static_cast<T>(*std::min_element(begin, end) - 1);
	// maxV is the largest element in s
	const T maxV = *std::max_element(begin, end);
	assert(minV < maxV); // just make sure the type conversion was successful

	std::vector<T> s(begin, end);
	for(size_t i=0; i<3; i++)
		s.emplace_back(minV); // append minV for edge cases

	std::vector<int> leftRank(n,-1);

	// phase 1
	SA left;
	{
		SA i_p;
		for(size_t i = 0; i < n; i++)
			if(i % 3 != 0)
				i_p.emplace_back(i);
		// radix sort triples
		for(int d=2; d>=0; d--)
			bucketSort(size_t(maxV - minV) + 1, i_p.begin(), i_p.end(), [&](auto i) {
					return size_t(s[i+d]-minV);
				});
		std::vector<int> lexicographic_names;
		{ // calculate lexicographic names
			int pre = 0, previous = -1;
			for(const auto i : i_p) {
				if(previous!=-1 && !std::equal(s.data()+i, s.data()+i+3, s.data()+previous))
					pre++; // S[i..i+2] is different from S[previous..previous+2]
				lexicographic_names.emplace_back(pre);
				previous = i;
			}
		}

		std::vector<int> i_to_lex(n, -1); // map each i to the lexicographic name of S[i..]
		for(size_t ii = 0; ii < i_p.size(); ii++)
			i_to_lex[i_p[ii]] = lexicographic_names[ii];

		// compute S'
		std::vector<int> s_p;
		// and tau & inverse tau
		//std::vector<int> tau(n,-1);
		std::vector<int> inv_tau(n,-1);
		for(size_t st : {1,2})
			for(size_t i=st; i<n; i+=3) {
				assert(i_to_lex[i] != -1);
					
				//tau[i] = s_p.size();
				inv_tau[s_p.size()] = i;

				s_p.emplace_back(i_to_lex[i]);
			}

		// recursive SA_p
		// Note: this is done regardless of whether the triples are
		// pairwise distinct
		auto sa_p = skew(s_p.begin(), s_p.end());

		for(size_t k=0; k<sa_p.size(); k++) {
			const unsigned int j = sa_p[k];
			const int i = inv_tau[j];

			leftRank[i] = left.size();
			left.emplace_back(i);
		}
	}

	// phase 2
	SA right;
	{
		for(size_t i=0; i<n; i+=3)
			right.emplace_back(i);
		// radix sort
		// first: sort by rank of S[i+1..n] (known from phase 1)
		bucketSort(n+1, right.begin(), right.end(), [&](auto i) {
				// smallest possible rank for positions after the last character
				if(i+1 >= leftRank.size()) return 0;
				else { // use rank of S[i+1..n] + 1
					assert(leftRank[i+1] != -1);
					return leftRank[i+1]+1;
				}
			});
		// second: sort by character S[i]
		bucketSort(size_t(maxV - minV) + 1, right.begin(), right.end(), [&](auto i) {
				return size_t(s[i]-minV);
			});
	}

	// phase 3: merge
	SA result;
	{
		size_t l=0, r=0;
		while(l < left.size() && r < right.size()) {
			size_t li = left[l], ri = right[r];
			int cmp = 0; // left : <0, right : >0
			// while no different chars have been found and
			// we are not in the left list, go to the next chars
			while(cmp==0 && (ri%3==0 || li%3==0))
				if(s[li] != s[ri])
					cmp = (s[li] < s[ri] ? -1 : 1);
				else li++, ri++;
			if(cmp == 0) { // if order is still unknown
				assert(li%3 != 0 && ri%3 != 0);
				if(li >= leftRank.size()) cmp = -1;
				else if(ri >= leftRank.size()) cmp = 1;
				else {
					assert(leftRank[li] != -1 && leftRank[ri] != -1);
					// use the left rank
					cmp = leftRank[li] < leftRank[ri] ? -1 : 1;
				}
			}

			result.emplace_back(cmp==-1 ? left[l++] : right[r++]);
		}
		while(l < left.size()) result.emplace_back(left[l++]);
		while(r < right.size()) result.emplace_back(right[r++]);
	}
	assert(result.size() == n);
	return result;
}
