#pragma once

#include "util.h"
#include <map>
#include <set>

class RankSelect {
private:
	std::vector<unsigned int> count0;
	std::vector<unsigned int> pos0;
	std::vector<unsigned int> pos1;

	//std::vector<bool> bbb;

	unsigned int n_;
public:
	RankSelect() = default;
	template<typename It>
	RankSelect(It begin, It end);

	unsigned int n() const;
	// rank(i,b) returns the number of b's in
	// [0..b]
	unsigned int rank(int, bool) const;
	// select(i,b) returns the i-th position of
	// b (NOTE: i is 0-based)
	// returns unsigned int max if i is too large
	unsigned int select(unsigned int, bool) const;
};

template<typename T>
using RankSelects = std::map<T,RankSelect>;

template<typename It, typename T = ref_type_t<It>>
RankSelects<T> rankSelects(It,It);


template<typename It>
RankSelect::RankSelect(It begin, It end) {
	//bbb = std::vector<bool>(begin, end);
	for(n_ = 0; begin != end; n_++,++begin) {
		if(*begin)
			pos1.emplace_back(n_);
		else
			pos0.emplace_back(n_);

		count0.emplace_back(pos0.size());
	}
}

template<typename It, typename T = ref_type_t<It>>
RankSelects<T> rankSelects(It begin, It end) {
	const unsigned int n = std::distance(begin, end);
	std::set<T> ts(begin, end);
	std::map<T, std::vector<bool>> bools;
	for(const auto& v : ts) bools[v].resize(n, false);
	for(unsigned int i = 0; begin != end; i++)
		bools.at(*(begin++))[i] = true;
	RankSelects<T> result;
	for(const auto&[t,b] : bools)
		result.emplace(t, RankSelect(b.begin(), b.end()));
	return result;
}
