#pragma once

#include <vector>
#include <tuple>
#include <iostream>
#include <cassert>
#include <type_traits>

// Zeug zum Ausgeben von vector, tuple und pair
template<typename...Ts>
std::tuple<Ts...> rd() {
	std::tuple<Ts...> res;
	std::apply([&](Ts &... ta) { ((std::cin >> ta), ...); }, res);
	return res;
}
template<typename T>
std::ostream& operator<<(std::ostream&, const std::vector<T>&);
template<typename... Ts>
std::ostream& operator<<(std::ostream&, std::tuple<Ts...> const&);
template<typename A, typename B>
std::ostream& operator<<(std::ostream&, std::pair<A,B>const&);

// smallest i in [lo,hi) where ok(i)=false (or hi if ok(i)=true for all i)
// assumes (i<j && ok(j)) => ok(i)
template<typename F>
int m_upper_bound(int lo, int hi, F ok);

template<typename T>
std::vector<T> inverse(const std::vector<T>& v);

template<typename It>
struct ref_type {
	typedef std::remove_cv_t<std::remove_reference_t<decltype(*std::declval<It>())>> type;
};
template<typename It>
using ref_type_t = typename ref_type<It>::type;

template<typename T>
std::ostream& operator<<(std::ostream&out, const std::vector<T>& v) {
	out << "{";
	for(size_t i=0; i<v.size(); i++) out << (i>0 ? ", " : "") << v[i];
	return out << "}";
}
template<typename... Ts>
std::ostream& operator<<(std::ostream& os, std::tuple<Ts...> const& theTuple) {
	std::apply([&os](Ts const&... ta) {
		size_t n{0};
		((os << (n++ != 0 ? ", " : "(") << ta), ...);
		os << ')';
	}, theTuple);
	return os;
}
template<typename A, typename B>
std::ostream& operator<<(std::ostream& out, std::pair<A,B>const& p) {
	return out << "<" << p.first << "," << p.second << ">";
}

#define DBG(s) #s << "=" << s

template<typename F>
int m_upper_bound(int lo, int hi, F ok) {
	if(!ok(lo)) return lo;
	while(lo + 1 < hi) {
		const int m = lo + (hi - lo) / 2;
		if(ok(m))	lo = m;
		else		hi = m;
	}
	return hi;
}

template<typename T>
std::vector<T> inverse(const std::vector<T>& v) {
	std::vector<T> res(v.size());
	for(size_t i=0; i<v.size(); i++) {
		assert(v[i] >= 0 && v[i] < v.size());
		res[v[i]] = i;
	}
	return res;
}
