#include "skew.h"
#include "lcp.h"

int main() {
	const std::string s = "bbabbabcc";
	
	const auto sa = skew(s.begin(), s.end());
	const auto lcp = kasai(s.begin(), s.end(), sa);
	const auto isa = inverse(sa);

	{ // 3.3
		// length of longest common prefix of suffix sa[i] with s
		std::vector<size_t> lcp_(lcp.size());
		lcp_[isa[0]] = s.size();
		for(int i=isa[0]-1; i>=0; i--)
			lcp_[i] = std::min(lcp_[i+1], (size_t) lcp[i+1]);
		for(size_t i=isa[0]+1; i<lcp.size(); i++)
			lcp_[i] = std::min(lcp_[i-1], (size_t) lcp[i]);

		{ // Nur Ausgabe
			printf("  i  SA LCP LCP' S_{SA[i]}\n");
			for(size_t i=0; i<s.size(); i++)
				printf("%3lu %3u %3d  %3lu %s\n", i, sa[i], lcp[i], lcp_[i], s.c_str()+sa[i]);
			printf("%3lu     %3d\n", s.size(), lcp[s.size()]);
		}

		// find all prefix tandem repeats
		for(size_t k=1; k<s.size(); k++)
			if(lcp_[isa[k]] >= k)
				printf("prefix tandem repeat of length %ld: %s\n", k, s.substr(0,k).c_str());
		std::cout << std::endl;
	}


	{ // 3.4
		const std::string s1 = "abcabcdeabc";
		const std::string s2 = "efabcddsfg";
		const auto s = s1+s2;
		const auto sa = skew(s.begin(), s.end());
		const auto lcp = kasai(s.begin(), s.end(), sa);

		std::pair<int,unsigned int> res(0,0);
		for(size_t i=0; i+1<sa.size(); i++)
			if((sa[i]<s1.size()) != (sa[i+1]<s1.size()))
				res = std::max(res, std::make_pair(lcp[i+1], std::min(sa[i],sa[i+1])));
		std::cout << "longest common substring: " << s1.substr(res.second, res.first) << std::endl;
	}
	return 0;
}
