#include "search.h"
#include "waveletTree.h"
#include <string.h>

int main() {
	const char* c_ = "acaaacacac$";
	const std::vector<char> s(c_, c_+strlen(c_));

	const auto bwt = burrows_wheeler(s);

	std::cerr << "bwt= " << bwt << std::endl;

	const std::string pattern = "acatt";
	std::cerr << "search: " << backwardSearch(
			C_array(bwt.begin(), bwt.end()),
			rankSelects(bwt.begin(), bwt.end()),
			pattern.begin(), pattern.end()) << std::endl;

	if(0) {
		std::vector<bool> b(10);
		for(size_t i=0; i<b.size(); i++) b[i]=rand()%2;
		std::cerr << DBG(b) << std::endl;
		RankSelect rs(b.begin(), b.end());
		for(size_t i=0; i<b.size(); i++) {
			std::cerr << b[i] << " : " << rs.rank(i, false) << " " << rs.rank(i, true) << std::endl;
		}
	}

	WaveletTree<char> t(bwt.begin(), bwt.end());

	{
		std::map<char,size_t> cnt;
		for(char c : bwt) cnt[c]=0;
		for(size_t i=0; i<bwt.size(); i++) {
			assert(t.select(bwt[i],cnt[bwt[i]]) == i);
			cnt[bwt[i]]++;
			for(const auto&[c,cc] : cnt)
				assert(t.rank(c, i) == cc);
		}
	}

	std::cerr << DBG(t.rank('a', 0)) << std::endl;
	std::cerr << DBG(t.rank('a', 1)) << std::endl;
	std::cerr << DBG(t.rank('a', 2)) << std::endl;
	std::cerr << DBG(t.rank('a', 3)) << std::endl;
	std::cerr << DBG(t.rank('a', 4)) << std::endl;
	std::cerr << DBG(t.rank('a', 5)) << std::endl;
	std::cerr << DBG(t.rank('a', 6)) << std::endl;
	std::cerr << DBG(t.rank('a', 7)) << std::endl;
	std::cerr << DBG(t.rank('a', 8)) << std::endl;
	std::cerr << DBG(t.rank('a', 9)) << std::endl;
	std::cerr << DBG(t.rank('a', 10)) << std::endl;

	std::cerr << t.select('a', 0) << std::endl;
	std::cerr << t.select('a', 1) << std::endl;
	std::cerr << t.select('a', 2) << std::endl;
	std::cerr << t.select('a', 3) << std::endl;
	std::cerr << t.select('a', 4) << std::endl;
	std::cerr << t.select('a', 5) << std::endl;

	return 0;
}
