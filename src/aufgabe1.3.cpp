#include "skew.h"

// iterate over chars & narrow interval
size_t countOcc(const std::string& s, const std::string& pat, const std::vector<unsigned int>& sa) {
	assert(sa.size() == s.size());

	// [lo,hi)
	size_t lo = 0, hi = s.size();
	for(size_t i = 0; i < pat.size(); i++) {
		const char c = pat[i];

		std::function<bool(bool,size_t)> cmp = [&](bool strict, size_t i_sa) {
				const size_t s_i = sa[i_sa] + i;
				if(s_i >= s.size()) return true;
				if(strict)	return s[s_i] < c;
				else		return s[s_i] <= c;
			};
		lo = m_upper_bound(lo, hi, std::bind(cmp, true, std::placeholders::_1));
		hi = m_upper_bound(lo, hi, std::bind(cmp, false, std::placeholders::_1));
	}

	return (hi > lo) ? hi-lo : 0;
}
size_t countOcc(const std::string& s, const std::string& pat) {
	return countOcc(s, pat, skew(s.begin(), s.end()));
}

// directly binary search over whole string
size_t countOcc2(const std::string& s, const std::string& pat, const std::vector<unsigned int>& sa) {
	assert(sa.size() == s.size());

	// find lower bound, first not less than pat
	size_t lo = m_upper_bound(0, s.size(), [&](size_t i) {
			return std::string(s.begin()+sa[i], min(s.begin()+sa[i]+pat.size(), s.end())) < pat;
		});
	// find upper bound, first greater than pat
	size_t hi = m_upper_bound(lo, s.size(), [&](size_t i) {
			return std::string(s.begin()+sa[i], min(s.begin()+sa[i]+pat.size(), s.end())) <= pat;
		});

	return (hi > lo) ? hi-lo : 0;
}
size_t countOcc2(const std::string& s, const std::string& pat) {
	return countOcc2(s, pat, skew(s.begin(), s.end()));
}

int main() {
	assert(countOcc("ctaataatg", "taat") == countOcc2("ctaataatg", "taat"));
	std::cout << countOcc("ctaataatg", "taat") << std::endl;

	return 0;
}
