#include "lcp.h"
#include "rmq.h"

#include <algorithm>

lcp_interval_tree build_lcp_tree(const std::vector<int>& lcp) {
	return build_lcp_tree(lcp, [](auto&){});
}
