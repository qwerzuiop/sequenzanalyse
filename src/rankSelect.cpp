#include "rankSelect.h"

#include <limits>

unsigned int RankSelect::n() const {
	return n_;
}

unsigned int RankSelect::rank(int i, bool b) const {
	i = std::min(i, static_cast<int>(count0.size()-1));

	//std::cerr << "rank " << DBG(i) << " " << DBG(b) << " " << bbb << " : ";

	if(i < 0) return 0;
	if(b) {
		//std::cerr << (i+1-count0[i]) << std::endl;
		return i+1-count0[i];
	} else {
		//std::cerr << count0[i] << std::endl;
		return count0[i];
	}
}

unsigned int RankSelect::select(unsigned int i, bool b) const {
	auto& a = b ? pos1 : pos0;
	if(i >= a.size())
		return std::numeric_limits<unsigned int>::max();
	return a[i];
}
