#include "skew.h"
#include "bwt.h"

// Aufgabe 5
template<typename T>
std::pair<SA,std::vector<T>> alg15(const std::vector<T>& bwt,
		const std::vector<unsigned int>& lf) {
	const auto n = bwt.size();
	size_t j = std::distance(lf.begin(), std::find(lf.begin(), lf.end(), 0));
	std::vector<T> res(n);
	SA sa(n,-1);
	// Lemma 2.1.4
	for(int i = n-1; i>=0; i--) {
		res[i] = bwt[j];
		j = lf[j];
		sa[j] = i;
	}
	return {sa, res};
}

int main() {
	{ // Aufgabe 4
		std::string s = "mmnm$uululi___";
		std::cout << "reconstructed: " << reconstruct_from_BWT(std::vector<char>(s.begin(), s.end())) << std::endl;
	}
	{ // Aufgabe 5
		std::string s = "mmnm$uululi___";
		std::vector<char> bwt(s.begin(), s.end());
		auto[sa,original] = alg15(bwt, last_to_first(bwt.begin(),bwt.end()));
		std::cout << "reconstructed " << original << std::endl;
		std::cout << "sa' = " << sa << std::endl;
		std::cout << "sa = " << skew(original.begin(), original.end()) << std::endl;
	}
	return 0;
}
