#include "skew.h"
#include "lcp.h"

// Aufgabe 2.3

// [i,j)
void findIndices(size_t i, size_t j, int l, const RMQ<int>& rmq, std::vector<size_t>& res) {
	if(i >= j) return;
	const size_t k = rmq.query(i+1, j);
	if(rmq.at(k) != l) return;
	if(k>0) findIndices(i, k-1, l, rmq, res);
	res.emplace_back(k);
	findIndices(k, j, l, rmq, res);
}

std::vector<std::pair<size_t,size_t>> childIntervals(size_t i, size_t j, const RMQ<int>& rmq) {
	const auto l = rmq.at(rmq.query(i+1,j));
	std::vector<size_t> l_indices;
	findIndices(i, j, l, rmq, l_indices);

	size_t last = i;
	std::vector<std::pair<size_t,size_t>> res;
	for(const auto& k : l_indices) {
		res.emplace_back(last, k-1);
		last = k;
	}
	res.emplace_back(last, j);
	return res;
}

int main() {
	// Aufgabe 2.1
	const std::string s = "annaassannasananas$";
	const auto sa = skew(s.begin(), s.end());
	printf("  i  SA S_{SA[i]}\n");
	for(size_t i=0; i<s.size(); i++)
		printf("%3lu %3u %s\n", i, sa[i], s.c_str()+sa[i]);

	std::cout << std::endl;
	// Aufgabe 2.2
	const auto lcp = kasai(s.begin(), s.end(), sa);
	const RMQ rmq_lcp(lcp);
	printf("  i  SA LCP S_{SA[i]}\n");
	for(size_t i=0; i<s.size(); i++)
		printf("%3lu %3u %3d %s\n", i, sa[i], lcp[i], s.c_str()+sa[i]);
	printf("%3lu     %3d\n", s.size(), lcp[s.size()]);

	build_lcp_tree(lcp, [&](auto& interval) {
			std::cout << std::make_pair(interval.lcp,
					std::make_pair(interval.lb, interval.rb)) << std::endl;
		});

	std::cout << std::endl;
	// Aufgabe 2.3
	std::cout << "children of <0," << s.size()-1 << "> : " << childIntervals(0, s.size()-1, rmq_lcp) << std::endl;


	return 0;
}
